# Deliasse-API

> Deliasse-API is a GraphQL server for French Parliament open data.

[Eliasse](http://eliasse.assemblee-nationale.fr/) is a web application developed by the French National Assembly, that allows real-time monitoring of amendments.

Deliasse-API is a module of the french [Open Parliament Platform](https://parlement-ouvert.fr).

Deliasse-API is free and open source software, developped by the french Member of Parliament [Paula Forteza](https://forteza.fr) and her team.

## Usage

TODO