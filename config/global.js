/**
 * Deliasse-API -- A GraphQL server for French Parliament open data
 * By: Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/deliasse-api
 *
 * Deliasse-API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Deliasse-API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from "path"

export default {
  ameliDb: {
    database: process.env.TRICOTEUSES_AMELI_DB_NAME || "ameli",
    host: process.env.TRICOTEUSES_AMELI_DB_HOST || "localhost",
    password: process.env.TRICOTEUSES_AMELI_DB_PASSWORD || "opendata",
    port: process.env.TRICOTEUSES_AMELI_DB_PORT || 5432,
    user: process.env.TRICOTEUSES_AMELI_DB_USER || "opendata",
  },
  anDb: {
    database: process.env.TRICOTEUSES_AN_DB_NAME || "parlapi",
    host: process.env.TRICOTEUSES_AN_DB_HOST || "localhost",
    password: process.env.TRICOTEUSES_AN_DB_PASSWORD || "parlapi",
    port: process.env.TRICOTEUSES_AN_DB_PORT || 5432,
    user: process.env.TRICOTEUSES_AN_DB_USER || "parlapi",
  },
  contact: {
    // email:
    name: process.env.TRICOTEUSES_CONTACT || "Deliasse-API Team",
    // url:
  },
  data: process.env.TRICOTEUSES_DATA || "data",
  description: process.env.TRICOTEUSES_DESCRIPTION || "GraphQL server for French Parliament open data",
  license: {
    // API license (not software license)
    name: "Apache 2.0",
    url: "http://www.apache.org/licenses/LICENSE-2.0",
  },
  listen: {
    host: null, // Listen to every IPv4 addresses.
    port: null, // Listen to config.port by default
  },
  port: process.env.TRICOTEUSES_PORT || 2048,
  proxy: process.env.TRICOTEUSES_PROXY || false, // Is this application used behind a trusted proxy?
  title: process.env.TRICOTEUSES_TITLE || "Deliasse-API",
  wsUrl: process.env.TRICOTEUSES_WS || "ws://localhost:2048",
}
