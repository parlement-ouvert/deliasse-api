/**
 * Deliasse-API -- A GraphQL server for French Parliament open data
 * By: Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/deliasse-api
 *
 * Deliasse-API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Deliasse-API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import fs from "fs"
import { PubSub, withFilter } from "graphql-subscriptions"
import { makeExecutableSchema } from "graphql-tools"
import GraphQLJSON from "graphql-type-json"
import path from "path"

import config from "../config"
import { ameliDb, ameliDbSharedConnectionObject, anDb, anDbSharedConnectionObject } from "../databases"

const pubsub = new PubSub()
const typeDefs = `
  scalar JSON

  enum Etat {
    AC  # accepté
    DI  # discuté
  }
  # enum SortEnSeance {
  #   Adopté
  #   Non soutenu
  #   Rejeté
  #   Retiré
  #   Tombé
  # }
  # enum TypeDiscussion {
  #   projet de loi
  # }

  "Amendement"
  type AmeliAmd {
    "Identifiant"
    id: Int!
    "Subdivision amendée"
    sub: AmeliSub
    "Identifiant de la subdivision amendée"
    subid: Int
    "Identifiant de l'amendement pere pour les sous-amendements"
    amdperid: Int
    "Identifiant de la motion"
    motid: Int
    "Identifiant de l'etat de l'amendement"
    etaid: Int!
    "Entité au nom de laquelle est déposé l'amendement"
    noment: AmeliEnt
    "Identifiant de l'entité au nom de laquelle est déposé l'amendement"
    nomentid: Int!
    "Sort de l'amendement"
    sor: AmeliSor
    "Identifiant du sort de l'amendement"
    sorid: String
    "Avis de la commission"
    avc: AmeliAvi
    "Identifiant de l'avis de la commission"
    avcid: String
    "Avis du gouvernement"
    avg: AmeliAvi
    "Identifiant de l'avis du gouvernement"
    avgid: String
    "Identifiant du type d'irrecevabilite"
    irrid: Int
    "Texte amendé"
    txt: AmeliTxt!
    "Identifiant du texte amendé"
    txtid: Int!
    "Identifiant de l'orateur pour (motion)"
    opmid: Int
    "Identifiant de l'orateur contre (motion)"
    ocmid: Int
    "Identifiant d'amendements identiques"
    ideid: Int
    "Identifiant d'amendements en discussion commune"
    discomid: Int
    "Numero de l'amendement (avec prefixe)"
    num: String
    "Niveau de rectification"
    rev: Int!
    "Type d'amendement"
    typ: String!
    "Dispositif de l'amendement"
    dis: String
    "Objet de l'amendement"
    obj: String
    "Date de depot de l'amendement"
    datdep: String
    "Observations ou commentaires sur l'amendement (1/2)"
    obs: String
    "Position au sein de l'article"
    ord: Int
    "Indication de la mention -Et plusieurs de ses collegues-"
    autext: Boolean!
    "Identification des amendements portant sur article additionnel (si different de 0)"
    subpos: Int
    "Observations ou commentaires sur l'amendement (2/2)"
    mot: String
    "Numero absolu de l'amendement diffuse (en chiffre)"
    numabs: Int
    "Identification de la subdivision de discussion"
    subidder: Int
    "Libelle complementaire (type d'appartenance au groupe)"
    libgrp: String
    "Numero du premier alinea modifie par l'amendement"
    alinea: Int
    "Amendement depose avec l'accord du gouvernement"
    accgou: Boolean!
    "Indication de la mention -Et plusieurs de ses collègues- (uniquement pour les amendements de commission)"
    colleg: Boolean!
    "Identifiant du type de rectification"
    typrectid: Int
    islu: Boolean!
    "Endroit ou de la motion qui sera examinee. M pour le derouleur de la motion. G pour la discussion generale"
    motposexa: String!
  }

  "Avis de la commission ou du gouvernement"
  type AmeliAvi {
    "Identifiant"
    id: String!
    "Libellé"
    lib: String!
    "Code"
    cod: String!
  }

  "Gouvernement (B)"
  type AmeliCab implements AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
    "Identifiant"
    entid: Int!
    "Code"
    codint: String!
    "Libellé"
    lil: String
  }

  "Commission (C)"
  type AmeliCom implements AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
    "Identifiant"
    entid: Int!
    "Code"
    cod: String!
    "Libellé"
    lib: String!
    "Libellé long"
    lil: String!
    "Ordre de presentation dans les listes"
    spc: String!
    "Code interne"
    codint: String!
    "Identifiant"
    tri: Int
  }

  "Entité"
  interface AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
  }

  "Groupe politique"
  type AmeliGrpPol implements AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
    "Identifiant"
    entid: Int!
    "Code"
    cod: String!
    "Libelle courant"
    libcou: String!
    "Libelle long courant"
    lilcou: String!
    "Code interne"
    codint: String!
    "Ordre de presentation dans les listes"
    tri: Int
  }

  "Membres gouvernement"
  type AmeliGvt implements AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
    "Identifiant"
    entid: Int!
    "Nom"
    nom: String!
    "Prenom"
    pre: String!
    "Qualite"
    qua: String!
    "Titre"
    tit: String!
  }

  "Nature des textes"
  type AmeliNat {
    "Identifiant"
    id: Int!
    "Libellé"
    lib: String!
  }

  "Liste des sénateurs"
  type AmeliSen implements AmeliEnt {
    "Identifiant"
    id: Int!
    "Type"
    typ: String!
    "Actif ou non"
    act: String!
    "Identifiant"
    entid: Int!
    "Groupe"
    grp: AmeliGrpPol!
    "Identifiant du groupe"
    grpid: Int!
    "Identifiant de la commission"
    comid: Int
    "Identifiant de la commission spéciale"
    comspcid: Int
    "Matricule"
    mat: String!
    "Qualité"
    qua: String!
    "Nom usuel"
    nomuse: String!
    "Prénom usuel"
    prenomuse: String!
    "Nom technique"
    nomtec: String
    "Indication de senateurs homonymes"
    hom: String
    "Indication de senateurs apparentes"
    app: String
    "Indication de senateurs rattache a un groupe"
    ratt: String
    "Nom usuel en minuscule"
    nomusemin: String!
    "Indication de feminisation des titres"
    senfem: String
  }

  "Session parlementaire"
  type AmeliSes {
    "Identifiant"
    id: Int!
    "Type de session"
    typid: Int!
    "Annee de session"
    ann: Int!
    "Libelle long"
    lil: String!
  }

  "Sort"
  type AmeliSor {
    "Identifiant"
    id: String!
    "Libelle"
    lib: String!
    "Code"
    cod: String!
    "Type"
    typ: String!
  }

  "Subdivision"
  type AmeliSub {
    "Identifiant"
    id: Int!
    "Identifiant du texte"
    txtid: Int!
    "Identifiant de la subdivision mere"
    merid: Int
    "Type"
    typ: AmeliTypSub
    "Identifiant du type"
    typid: Int
    "Libelle court"
    lic: String
    "Libelle long"
    lib: String
    "Position dans le texte"
    pos: Int
    "Nom du signet"
    sig: String
    "Position dans la discussion"
    posder: Int
    "Indicateur de subdivision mise en reserve ou discutee en priorite"
    prires: Int
    "Indicateur de subdivision dupliquee"
    dupl: String!
    "Indicateur de subdivision amendable"
    subamd: String!
    sorid: String
    "ID du dérouleur texte"
    txtidder: Int
    "Style d'affichage dans le dérouleur"
    style: String!
  }

  "Texte amende"
  type AmeliTxt {
    "Identifiant"
    id: Int!
    "Nature du texte"
    nat: AmeliNat!
    "Identifiant de la nature du texte"
    natid: Int!
    "Identifiant de lecture"
    lecid: Int!
    "Session d'inscription"
    sesins: AmeliSes
    "Identifiant de la session d'inscription"
    sesinsid: Int
    "Session de dépôt"
    sesdep: AmeliSes!
    "Identifiant de la session de depot"
    sesdepid: Int!
    "Identifiant de la mission (si texte de la loi de finance)"
    fbuid: Int
    "Numero du texte"
    num: String!
    "Intitule du texte"
    int: String!
    "Intitule long du texte"
    inl: String
    "Date de depot"
    datdep: String!
    "Urgence (avant la revision constitutionnelle de 2008)"
    urg: String!
    "Indicateur de texte disponible"
    dis: String!
    "Indicateur de texte en seconde deliberation"
    secdel: String!
    "Texte du projet de loi de finance"
    loifin: String!
    "Indicateur de la partie du projet de loi de finance"
    loifinpar: Int
    "Indicateur de texte amendable"
    txtamd: String!
    "Date d'adoption du texte"
    datado: String
    "Numero d'adoption du texte"
    numado: Int
    "Indicateur de texte examine"
    txtexa: String
    "Publication du delai limite"
    pubdellim: String
    "Numero du texte (en chiffre)"
    numabs: Int
    "Libelle du delai limite"
    libdelim: String
    "Libelle complementaire de la nature du texte"
    libcplnat: String
    "Signet du dossier legislatif"
    doslegsignet: String
    "Procedure acceleree (depuis la reforme constitutionnelle de 2008)"
    proacc: String!
    "Type du texte"
    txttyp: String!
    "Ordre de la seconde deliberation"
    ordsnddelib: String
    "Identifiant de l'etat du texte"
    txtetaid: Int!
    "ID du dérouleur fusionné"
    fusderid: Int
    "O sil s'agit dun  dérouleur fusionné, N sinon"
    fusder: String!
    "Ordre de discussion des textes dans un dérouleur fusionné"
    fusderord: Int!
    "Type de fusion dérouleur DG ou DA"
    fusdertyp: String
    "Amendements du texte"
    amds: [AmeliAmd!]!
    "Subdivisions du texte"
    subs: [AmeliSub!]!
  }

  "Types de subdivision"
  type AmeliTypSub {
    "Identifiant"
    id: Int!
    "Libellé"
    lib: String!
  }

  "Dossier législatif de l'Assemblée"
  type AnDossier {
    id: String!
    titre: String!
    "Titre simplifié, pour être utilisé dans le chemain d'une URL"
    titre_chemin: String
    "URL du dossier législatif au Sénat"
    senat_chemin: String
    procedure_code: Int!
    procedure_libelle: String!
    "Numéro de la législature"
    legislature_id: Int!
  }

  type Amendement {
    auteur: Auteur!
    bibard: String!
    bibardSuffixe: String!
    cosignataires: [Auteur!]!
    cosignatairesMentionLibre: CosignatairesMentionLibre
    dispositif: String
    etat: Etat!
    exposeSommaire: String
    legislature: Int!
    numero: String!
    numeroLong: String!
    numeroReference: String!
    organeAbrv: String!
    place: String
    placeReference: String!
    sortEnSeance: String  # SortEnSeance
  }
  type Auteur {
    civilite: String!
    estRapporteur: Boolean!
    estGouvernement: Boolean!
    groupeTribunId: String!
    nom: String!
    photoUrl: String!
    prenom: String!
    qualite: String
    tribunId: String!
  }
  type CosignatairesMentionLibre {
    titre: String!
  }
  type Discussion {
    amendements: [EnveloppeAmendement!]!
    bibard: String!
    bibardSuffixe: String!
    divisions: [Division!]!
    legislature: Int!
    numeroProchainADiscuter: String
    organe: String!
    pages: [Page!]!
    titre: String!
    type: String!  # TypeDiscussion!
  }
  type Division {
    # amendements:
    place: String!
    position: String!
  }
  type ElementOrdreDuJour {
    textBibard: String!
    textBibardSuffixe: String!
    textTitre: String!
  }
  type EnveloppeAmendement {
    alineaLabel: String
    amendement: Amendement
    auteurGroupe: String
    auteurLabel: String
    discussionCommune: String
    discussionCommuneAmdtPositon: String
    discussionCommuneSsAmdtPositon: String
    discussionIdentique: String
    discussionIdentiqueAmdtPositon: String
    discussionIdentiqueSsAmdtPositon: String
    missionLabel: String
    numero: String
    parentNumero: String
    place: String
    position: String
    sort: String  # SortEnSeance
  }
  type Page {
    body: String!
    numero: String!
    slug: String!
    titre: String
  }
  type ProchainADiscuter {
    bibard: String!
    bibardSuffixe: String!
    legislature: Int!
    nbrAmdtRestant: Int!
    numAmdt: String!
    organeAbrv: String!
  }
  type ReferenceOrgane {
    text: String!
    value: String!
  }

  # The schema allows the following query:
  type Query {
    amendement(bibard: String!, bibardSuffixe: String, legislature: Int!, numero: String!, organe: String!): Amendement
    assembleeDossiers(legislature: Int!): [AnDossier!]!
    discussion(bibard: String!, bibardSuffixe: String, legislature: Int!, organe: String!): Discussion
    ordreDuJour(legislature: Int!, organe: String!): [ElementOrdreDuJour!]
    referencesOrganes(legislature: Int!): [ReferenceOrgane]!
    senatDiscussion(num: String!, sesinsLil: String!, txttyp: String!): AmeliTxt
  }
  # The schema allows the following mutations:
  # type Mutation {
  #   upvoteStatement (
  #     statementId: String!
  #   ): Statement
  # }
  type Subscription {
    ameliAmdUpserted : AmeliAmd
    amendementUpserted : Amendement
    prochainADiscuterUpserted : ProchainADiscuter
  }
`
const resolvers = {
  AmeliAmd: {
    async avc(amd) {
      return amd.avcid === null
        ? null
        : entryToAmeliAvi(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM avicom
                WHERE id = $1
              `,
              amd.avcid
            )
          )
    },
    async avg(amd) {
      return amd.avgid === null
        ? null
        : entryToAmeliAvi(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM avigvt
                WHERE id = $1
              `,
              amd.avgid
            )
          )
    },
    async noment(amd) {
      const ent =
        amd.nomentid === null || amd.nomentid === 0
          ? null
          : await ameliDb.oneOrNone(
              `
                SELECT *
                FROM ent
                WHERE id = $1
              `,
              amd.nomentid
            )
      if (ent === null) return null
      switch (ent.typ) {
        case "B":
          return entryToAmeliCab({
            ...ent,
            ...(await ameliDb.oneOrNone(
              `
                SELECT *
                FROM cab
                WHERE entid = $1
              `,
              ent.id
            )),
          })
        case "C":
          return entryToAmeliCom({
            ...ent,
            ...(await ameliDb.oneOrNone(
              `
                SELECT *
                FROM com_ameli
                WHERE entid = $1
              `,
              ent.id
            )),
          })
        case "E":
          return entryToAmeliSen({
            ...ent,
            ...(await ameliDb.oneOrNone(
              `
                SELECT *
                FROM sen_ameli
                WHERE entid = $1
              `,
              ent.id
            )),
          })
        case "G":
          return entryToAmeliGrpPol({
            ...ent,
            ...(await ameliDb.oneOrNone(
              `
                SELECT *
                FROM grppol_ameli
                WHERE entid = $1
              `,
              ent.id
            )),
          })
        case "M":
          return entryToAmeliGvt({
            ...ent,
            ...(await ameliDb.oneOrNone(
              `
                SELECT *
                FROM gvt
                WHERE entid = $1
              `,
              ent.id
            )),
          })
        default:
          console.log(`Unknown AmeliEnt.typ: ${ent.typ}`)
          return entryToAmeliEnt(ent)
      }
    },
    async sor(amd) {
      return amd.sorid === null
        ? null
        : entryToAmeliSor(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM sor
                WHERE id = $1
              `,
              amd.sorid
            )
          )
    },
    async sub(amd) {
      return amd.subid === null
        ? null
        : entryToAmeliSub(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM sub
                WHERE id = $1
              `,
              amd.subid
            )
          )
    },
    async txt(amd) {
      return amd.txtid === null
        ? null
        : entryToAmeliTxt(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM txt_ameli
                WHERE id = $1
              `,
              amd.txtid
            )
          )
    },
  },
  AmeliEnt: {
    __resolveType(ent) {
      switch (ent.typ) {
        case "B":
          return "AmeliCab"
        case "C":
          return "AmeliCom"
        case "E":
          return "AmeliSen"
        case "G":
          return "AmeliGrpPol"
        case "M":
          return "AmeliGvt"
        default:
          console.log(`Unknown AmeliEnt.typ: ${ent.typ}`)
          return null
      }
    },
  },
  AmeliSen: {
    async grp(sen) {
      return sen.grpid === null
        ? null
        : entryToAmeliGrpPol(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM ent
                INNER JOIN grppol_ameli
                ON id = entid
                WHERE id = $1
              `,
              sen.grpid
            )
          )
    },
  },
  AmeliSub: {
    async typ(sub) {
      return sub.typid === null
        ? null
        : entryToAmeliTypSub(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM typsub
                WHERE id = $1
              `,
              sub.typid
            )
          )
    },
  },
  AmeliTxt: {
    async amds(txt) {
      return (await ameliDb.any(
        `
          SELECT *
          FROM amd
          WHERE txtid = $1
        `,
        txt.id
      )).map(entryToAmeliAmd)
    },
    async nat(txt) {
      return txt.natid === null
        ? null
        : entryToAmeliNat(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM nat
                WHERE id = $1
              `,
              txt.natid
            )
          )
    },
    async sesdep(txt) {
      return txt.sesdepid === null
        ? null
        : entryToAmeliSes(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM ses
                WHERE id = $1
              `,
              txt.sesdepid
            )
          )
    },
    async sesins(txt) {
      return txt.sesinsid === null
        ? null
        : entryToAmeliSes(
            await ameliDb.oneOrNone(
              `
                SELECT *
                FROM ses
                WHERE id = $1
              `,
              txt.sesinsid
            )
          )
    },
    async subs(txt) {
      return (await ameliDb.any(
        `
          SELECT *
          FROM sub
          WHERE txtid = $1
        `,
        txt.id
      )).map(entryToAmeliSub)
    },
  },
  JSON: GraphQLJSON,
  // Mutation: {
  // upvoteStatement: (_, { statementId }) => {
  //   const post = find(posts, { id: postId });
  //   if (!post) {
  //     throw new Error(`Couldn't find post with id ${postId}`);
  //   }
  //   post.votes += 1;
  //   return post;
  // },
  // },
  // Author: {
  //   posts: (author) => filter(posts, { authorId: author.id }),
  // },
  // Post: {
  //   author: (post) => find(authors, { id: post.authorId }),
  // },
  Query: {
    amendement(_, { bibard, bibardSuffixe, legislature, numero, organe }) {
      return readAmendement(bibard, bibardSuffixe, "assemblee", legislature, numero, organe)
    },
    async assembleeDossiers(_, { legislature }) {
      return (await anDb.any(
        `
          SELECT * FROM an_dossiers
          WHERE legislature_id = $<legislature>
        `,
        {
          legislature,
        }
      )).map(entryToAnDossier)
    },
    discussion(_, { bibard, bibardSuffixe, legislature, organe }) {
      const discussionText = fs.readFileSync(
        path.join(
          config.data,
          "assemblee",
          legislature.toString(),
          organe,
          bibard + (bibardSuffixe || ""),
          "discussion.json"
        )
      )
      const discussion = JSON.parse(discussionText)

      const prochainADiscuterFilePath = path.join(
        config.data,
        "assemblee",
        legislature.toString(),
        organe,
        "prochain-a-discuter.json"
      )
      let prochainADiscuterText
      try {
        prochainADiscuterText = fs.readFileSync(prochainADiscuterFilePath)
      } catch (err) {
        if (err.code === "ENOENT") {
          console.log(`File not found: ${prochainADiscuterFilePath}`)
          prochainADiscuterText = null
        } else {
          throw err
        }
      }
      let numeroProchainADiscuter = null
      if (prochainADiscuterText !== null) {
        const prochainADiscuter = JSON.parse(prochainADiscuterText)
        if (
          prochainADiscuter.bibard === bibard &&
          prochainADiscuter.bibardSuffixe === (bibardSuffixe || "") &&
          parseInt(prochainADiscuter.legislature) === legislature &&
          prochainADiscuter.organeAbrv === organe
        ) {
          numeroProchainADiscuter = prochainADiscuter.numAmdt
        }
      }
      discussion.numeroProchainADiscuter = numeroProchainADiscuter

      const pagesFilePath = path.join(
        config.data,
        "assemblee",
        legislature.toString(),
        organe,
        bibard + (bibardSuffixe || ""),
        "pages.json"
      )
      let pagesText
      try {
        pagesText = fs.readFileSync(pagesFilePath)
      } catch (err) {
        if (err.code === "ENOENT") {
          console.log(`File not found: ${pagesFilePath}`)
          pagesText = "[]"
        } else {
          throw err
        }
      }
      discussion["pages"] = JSON.parse(pagesText)

      return {
        ...discussion,
        amendements: discussion.amendements.map(enveloppeAmendement => ({
          ...enveloppeAmendement,
          amendement: readAmendement(
            bibard,
            bibardSuffixe,
            "assemblee",
            legislature,
            enveloppeAmendement.numero,
            organe
          ),
          auteurGroupe: enveloppeAmendement.auteurGroupe || null,
        })),
      }
    },
    ordreDuJour(_, { legislature, organe }) {
      const text = fs.readFileSync(
        path.join(config.data, "assemblee", legislature.toString(), organe, "ordre-du-jour.json")
      )
      return JSON.parse(text)
    },
    referencesOrganes(_, { legislature }) {
      const text = fs.readFileSync(path.join(config.data, "assemblee", legislature.toString(), "organes.json"))
      return JSON.parse(text)
    },
    async senatDiscussion(_, { num, sesinsLil, txttyp }) {
      return entryToAmeliTxt(
        await ameliDb.oneOrNone(
          `
          SELECT txt_ameli.* FROM txt_ameli
          INNER JOIN ses ON ses.id = txt_ameli.sesinsid
          WHERE txt_ameli.num = $<num> AND txt_ameli.txttyp = $<txttyp> AND ses.lil = $<sesinsLil>
        `,
          {
            num,
            sesinsLil,
            txttyp,
          }
        )
      )
    },
  },
  Subscription: {
    ameliAmdUpserted: {
      resolve: (amendement /* variables, context, info */) => {
        console.log("----> AmeliAmd", amendement.num, amendement.sorid)
        return amendement
      },
      subscribe: withFilter(
        (/* object, variables, context, info */) => {
          // console.log("Subscription.amendementUpserted.subscribe")
          // TODO: Add fs.watch is directory is not yet watched (=> count subscriptions for the same directory).
          return pubsub.asyncIterator("ameliAmdUpserted")
        },
        (/* object, variables, context, info */) => {
          return true
        }
      ),
    },
    amendementUpserted: {
      resolve: (amendement /* variables, context, info */) => {
        console.log("----> Amendement", amendement.numeroReference, amendement.sortEnSeance)
        return amendement
      },
      subscribe: withFilter(
        (/* object, variables, context, info */) => {
          // console.log("Subscription.amendementUpserted.subscribe")
          // TODO: Add fs.watch is directory is not yet watched (=> count subscriptions for the same directory).
          return pubsub.asyncIterator("amendementUpserted")
        },
        (/* object, variables, context, info */) => {
          return true
        }
      ),
    },
    prochainADiscuterUpserted: {
      resolve: (prochainADiscuter /* variables, context, info */) => {
        console.log("----> prochainADiscuter", prochainADiscuter.numAmdt)
        return prochainADiscuter
      },
      subscribe: withFilter(
        () => {
          // console.log("Subscription.prochainADiscuterUpserted.subscribe")
          // TODO: Add fs.watch is directory is not yet watched (=> count subscriptions for the same directory).
          return pubsub.asyncIterator("prochainADiscuterUpserted")
        },
        (/* object, variables, context, info */) => {
          return true
        }
      ),
    },
  },
}

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

function entryToAmeliAmd(entry) {
  return entry === null
    ? null
    : {
        ...entry,
        accgou: entry.accgou === "O",
        autext: entry.autext === "O",
        colleg: entry.colleg === "O",
        islu: entry.islu === "O",
      }
}

function entryToAmeliAvi(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliCab(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliCom(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliEnt(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliGrpPol(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliGvt(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliNat(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliSen(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliSes(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliSor(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliSub(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliTxt(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAmeliTypSub(entry) {
  return entry === null
    ? null
    : {
        ...entry,
      }
}

function entryToAnDossier(entry) {
  if (entry === null) return null
  entry = { ...entry }
  delete entry.search_vector
  return entry
}

export async function listenToAmeliAmdUpserted() {
  ameliDbSharedConnectionObject.client.on("notification", async data => {
    try {
      if (data.channel === "amendement_upserted") {
        const amendement = entryToAmeliAmd(
          await ameliDb.oneOrNone(
            `
            SELECT *
            FROM amd
            WHERE id = $1
          `,
            data.payload
          )
        )
        pubsub.publish("ameliAmdUpserted", amendement)
      } else {
        console.log(`Ignoring unknown channel "${data.channel}" (in notification: $}{data}).`)
      }
    } catch (error) {
      console.log(error.stack || error)
      process.exit(1)
    }
  })

  // Wait for new actions.
  await ameliDbSharedConnectionObject.none("LISTEN $1~", "amendement_upserted")

  console.log("Listening for ameli.amendement_upserted...")
}

function parseAmendementJson(text) {
  const amendement = JSON.parse(text)
  const auteur = amendement["auteur"]
  auteur["estGouvernement"] = Boolean(parseInt(auteur["estGouvernement"]))
  auteur["estRapporteur"] = Boolean(parseInt(auteur["estRapporteur"]))
  amendement.cosignataires = amendement.cosignataires.map(cosignataire => {
    cosignataire["estGouvernement"] = Boolean(parseInt(cosignataire["estGouvernement"]))
    cosignataire["estRapporteur"] = Boolean(parseInt(cosignataire["estRapporteur"]))
    return cosignataire
  })
  amendement["legislature"] = parseInt(amendement["legislature"])
  return amendement
}

function readAmendement(bibard, bibardSuffixe, chambre, legislature, numero, organe) {
  const amendementPath = path.join(
    config.data,
    "assemblee",
    legislature.toString(),
    organe,
    bibard + (bibardSuffixe || ""),
    "amendement-" + numero + ".json"
  )
  let text
  try {
    text = fs.readFileSync(amendementPath)
  } catch (err) {
    if (err.code === "ENOENT") {
      console.log(`File not found: ${amendementPath}`)
      return null
    } else {
      throw err
    }
  }
  return parseAmendementJson(text)
}

// const organeDir = path.join(config.data, "assemblee", "15", "CION_LOIS")
const organeDir = path.join(config.data, "assemblee", "15", "AN")
const organeWatcher = fs.watch(organeDir, (eventType, filename) => {
  console.log(`event type is: ${eventType}`)
  if (filename) {
    console.log(`filename provided: ${filename}`)
    const filePath = path.join(organeDir, filename)
    let text
    try {
      text = fs.readFileSync(filePath)
    } catch (err) {
      if (err.code === "ENOENT") {
        // pubsub.publish("amendementDeleted", amendementPath)
      } else {
        throw err
      }
    }
    let data
    try {
      data = JSON.parse(text)
    } catch (err) {
      console.log(`Ignoring parse error in JSON file ${filePath}`)
      data = null
    }
    if (data !== null) {
      if (filename === "prochain-a-discuter.json") {
        data["legislature"] = parseInt(data["legislature"])
        data["nbrAmdtRestant"] = parseInt(data["nbrAmdtRestant"])
        pubsub.publish("prochainADiscuterUpserted", data)
      }
    }
  } else {
    console.log("Ignoring fs.watch event: Filename not provided.")
  }
})
// organeWatcher.close()

// const discussionDir = path.join(organeDir, "490")
const discussionDir = path.join(organeDir, "592")
const discussionWatcher = fs.watch(discussionDir, (eventType, filename) => {
  console.log(`event type is: ${eventType}`)
  if (filename) {
    console.log(`filename provided: ${filename}`)
    const amendementPath = path.join(discussionDir, filename)
    let text
    try {
      text = fs.readFileSync(amendementPath)
    } catch (err) {
      if (err.code === "ENOENT") {
        pubsub.publish("amendementDeleted", amendementPath)
      } else {
        throw err
      }
    }
    let amendement
    try {
      amendement = parseAmendementJson(text)
    } catch (err) {
      console.log(`Ignoring parse error in JSON file ${amendementPath}`)
      amendement = null
    }
    if (amendement !== null) {
      pubsub.publish("amendementUpserted", amendement)
    }
  } else {
    console.log("Ignoring fs.watch event: Filename not provided.")
  }
})
// discussionWatcher.close()
