/**
 * Deliasse-API -- A GraphQL server for French Parliament open data
 * By: Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/deliasse-api
 *
 * Deliasse-API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Deliasse-API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import assert from "assert"
import pgPromiseFactory from "pg-promise"

import config from "./config"

const pgPromise = pgPromiseFactory()

export const ameliDb = pgPromise({
  database: config.ameliDb.database,
  host: config.ameliDb.host,
  password: config.ameliDb.password,
  port: config.ameliDb.port,
  user: config.ameliDb.user,
})
export let ameliDbSharedConnectionObject = null

export const anDb = pgPromise({
  database: config.anDb.database,
  host: config.anDb.host,
  password: config.anDb.password,
  port: config.anDb.port,
  user: config.anDb.user,
})
export let anDbSharedConnectionObject = null

export async function checkDatabases({ ignoreTextSearchVersion = false } = {}) {
  // Check that ameli database exists.
  ameliDbSharedConnectionObject = await ameliDb.connect()
  assert(await existsTable(ameliDb, "amd"), "Ameli database is not initialized.")

  // Check that an database exists.
  anDbSharedConnectionObject = await anDb.connect()
  assert(await existsTable(anDb, "an_dossiers"), "AN database is not initialized.")
}

async function existsTable(db, tableName) {
  return (await db.one("SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_name=$1)", [tableName]))
    .exists
}
