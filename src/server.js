/**
 * Deliasse-API -- A GraphQL server for French Parliament open data
 * By: Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/deliasse-api
 *
 * Deliasse-API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Deliasse-API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import bodyParser from "body-parser"
import cors from "cors"
import express from "express"
import { execute, subscribe } from "graphql"
import { graphiqlExpress, graphqlExpress } from "graphql-server-express"
import http from "http"
import { SubscriptionServer } from "subscriptions-transport-ws"

import config from "./config"
import { checkDatabases } from "./databases"
import * as graphqlController from "./controllers/graphql"

const app = express()

app.set("title", config.title)
app.set("trust proxy", config.proxy)

// Enable Express case-sensitive and strict options.
app.enable("case sensitive routing")
app.enable("strict routing")

app.use(cors())
app.use(bodyParser.json({ limit: "5mb" }))

app.use(
  "/graphiql",
  graphiqlExpress({
    endpointURL: "/graphql",
    subscriptionsEndpoint: `${config.wsUrl}/subscriptions`,
  })
)
app.use("/graphql", graphqlExpress({ schema: graphqlController.schema }))

checkDatabases()
  .then(async () => {
    startExpress()
    await graphqlController.listenToAmeliAmdUpserted()
  })
  .catch(error => {
    console.log(error.stack || error)
    process.exit(1)
  })

function startExpress() {
  const host = config.listen.host
  const port = config.listen.port || config.port
  const server = http.createServer(app)
  server.listen(port, host, () => {
    console.log(`Listening on ${host || "*"}:${port}...`)
    // Set up the WebSocket for handling GraphQL subscriptions.
    new SubscriptionServer(
      {
        execute,
        schema: graphqlController.schema,
        subscribe,
      },
      {
        server,
        path: "/subscriptions",
      }
    )
  })
  server.timeout = 30 * 60 * 1000 // 30 minutes (in milliseconds)
}
