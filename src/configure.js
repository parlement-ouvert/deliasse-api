/**
 * Deliasse-API -- A GraphQL server for French Parliament open data
 * By: Emmanuel Raviart <emmanuel@raviart.com>
 *
 * Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
 * https://framagit.org/parlement-ouvert/deliasse-api
 *
 * Deliasse-API is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Deliasse-API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import config from "./config"
import { ameliDb, anDb } from "./databases"

async function configureDatabases() {
  // Configure "ameli" database.

  await ameliDb.connect()

  await ameliDb.none(
    `
      CREATE OR REPLACE FUNCTION notify_amendement_upserted() RETURNS trigger AS $$
      BEGIN
        PERFORM pg_notify('amendement_upserted', CAST(NEW.id AS text));
        RETURN NEW;
      END;
      $$ LANGUAGE plpgsql
    `
  )
  try {
    await ameliDb.none("DROP TRIGGER amendement_upserted ON amd")
  } catch (e) {}
  await ameliDb.none(
    `
      CREATE TRIGGER amendement_upserted AFTER INSERT OR UPDATE ON amd
      FOR EACH ROW
      EXECUTE PROCEDURE notify_amendement_upserted()
    `
  )

  // Configure "an" database.

  await anDb.connect()
}
